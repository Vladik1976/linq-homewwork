﻿using LINQ_homework.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_homework.BLL.Services
{
    public class TeamService :BaseService
    {
        /// <summary>
        /// Get list of teams contains user over 10 years old
        /// </summary>
        /// <returns></returns>
        public List<Tuple<int,string,List<User>>> GetTeamsLimited()
        {
            var tasks = (from t in Teams
                         select new { t.Id, t.Name, Users = t.Users.Where(u => DateTime.Now.Year - u.Birthday.Value.Year > 10).OrderByDescending(b=>b.RegisteredAt).ToList() }
                        ).Where(x=>x.Users.Count>0)
                        .Select(t => new Tuple<int, string, List<User>>(t.Id, t.Name, t.Users)).ToList();
                        

            return tasks;

        }
    }
}
