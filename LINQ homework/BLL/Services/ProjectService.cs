﻿using LINQ_homework.DAL.Entities;
using System.Linq;

namespace LINQ_homework.BLL.Services
{
    class ProjectService: BaseService
    {
        public ProjectDetails GetProjectDetails(int projectId)
        {
            var projectdetails = (from p in Projects
                        where p.Id == projectId
                        select new ProjectDetails
                        {
                            Project = p,

                            LongestTask = p.Tasks.OrderByDescending(x=>x.Description).FirstOrDefault(),

                            ShortestTask = p.Tasks.OrderBy(x=>x.Name).FirstOrDefault(),

                            NumberOfPerformers = (from t in p.Tasks
                                                  where p.Description.Length>20 || p.Tasks.Count<3
                                                  select t.PerformerId).Distinct().Count()
                        }).FirstOrDefault();

            return projectdetails;

        }


    }
}
