﻿using System.Collections.Generic;
using LINQ_homework.DAL.Entities;

namespace LINQ_homework.BLL.Services
{
    public abstract class BaseService
    {
        public  List<Project> Projects { get; private set; }
        public  List<Task> Tasks { get; private set; }
        public  List<User> Users { get; private set; }
        public  List<Team> Teams { get; private set; }
        public BaseService()
        {
            Projects = Program.Projects;
            Tasks = Program.Tasks;
            Users = Program.Users;
            Teams = Program.Teams;
        }

       
    }
}
