﻿using LINQ_homework.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ_homework.BLL.Services
{
    public class UserService :BaseService
    {
        ///<summary>Get list of projects where at least one task assigned to the particular used
        ///Returns Disctionary</summary>
        ///
        public Dictionary<Project, int> GetProjects(int userId)
        {
            return base.Projects.Where(p => p.Tasks.Any(t => t.PerformerId == userId)).ToDictionary(p => p, p => p.Tasks.Where(t => t.PerformerId == userId).Count());
        }

        ///<summary>Get a list of tasks assigned to the particular user and which name length less than 45 chars</summary>
        ///
        public List<Task> GetTasks(int userId)
        {
            var tasks = Tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList();

            return tasks;
        }

        ///<summary>Get a list of tasks finished in 2021 by particular user</summary>
        ///
        public List<Tuple<int,string>> GetFinishedTasks(int userId)
        {
            var tasks = base.Tasks.Where(t => t.FinishedAt != null).Where(t => t.FinishedAt.Value.Year == 2021 && t.PerformerId == userId)
                .Select(t => new { t.Id, t.Name }).AsEnumerable()
                .Select(t => new Tuple<int, string>(t.Id, t.Name)).ToList();
            
            return tasks;
        }
        /// <summary>
        /// Returns Users with associated tasks
        /// </summary>
        /// <returns></returns>
        public List<Tuple<string,List<Task>>> GetUsersWithTasks()
        {
            var users = (from u in Users
                         select new { u.FirstName, Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList() }
                        ).Select(u => new Tuple<string, List<Task>>(u.FirstName, u.Tasks)).OrderBy(u => u.Item1).ToList();


            return users;
        }
        public UserDetails GetUserDetails (int userId)
        {
            var user = (from u in Users
                        where u.Id == userId
                        select new UserDetails
                        {
                            User = u,
                            Project = (from p in Projects
                                       where p.Tasks.ToList().Any(t => t.PerformerId == u.Id)
                                       select (p)).OrderByDescending(x => x.CreatedAt).FirstOrDefault(),

                            NumberOfTasks = (from p in Projects
                                             where p.Tasks.ToList().Any(t => t.PerformerId == u.Id)
                                             select (p)).OrderByDescending(x => x.CreatedAt).FirstOrDefault()?.Tasks.Count,

                            UnfinishedTasks = (from t in u.Tasks
                                               where t.PerformerId == u.Id && t.FinishedAt == null
                                               select (t)).Count(),

                            LongestTask = (from t in u.Tasks
                                           orderby (t.FinishedAt ?? DateTime.Now) - t.CreatedAt descending
                                           where t.PerformerId == u.Id
                                           select (t)).FirstOrDefault()
                        }).FirstOrDefault();

            return user;

        }
    }
}
