﻿using LINQ_homework.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;


namespace LINQ_homework.DAL.Entities
{
    public class Team:BaseEntity
    {
        public Team()
        {
            Users = new List<User>();
        }
        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<User> Users { get; set; }

    }
}
