﻿
namespace LINQ_homework.DAL.Entities
{
    public struct UserDetails
    {
        public User User { get; set; }

        public Project Project { get; set; }

        public int? NumberOfTasks { get; set; }

        public int UnfinishedTasks { get; set; }

        public Task LongestTask { get; set; }

    }
}
