﻿
namespace LINQ_homework.DAL.Entities
{
    public struct ProjectDetails
    {
        public Project Project { get; set; }

        public Task LongestTask { get; set; }

        public Task ShortestTask { get; set; }

        public int NumberOfPerformers { get; set; }

    }
}