﻿using System;

namespace LINQ_homework.DAL.Entities.Abstract
{
    public abstract class BaseEntity
    {

        public BaseEntity() { }
        

        public int Id { get; set; }

    }
}
