﻿using LINQ_homework.BLL.Services;
using LINQ_homework.DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace LINQ_homework
{
    class Program
    {
        private static HttpService _client;

        static void Main(string[] args)
        {
            _client = new HttpService(ConfigurationManager.AppSettings["EndPointURL"].ToString());
            
            PrepareData().Wait();

            BasicMenu();
        }

        private static void ProjectsByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();
            
            if (int.TryParse(UserId,out int Id))
            {
                UserService _userService = new UserService();

                var y = _userService.GetProjects(Id);
                foreach (KeyValuePair<Project, int> keyValue in y)
                {
                    Console.WriteLine($"Project {keyValue.Key.Name}  Tasks:{keyValue.Value.ToString()}");
                }
                
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }

        private static void TasksByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                UserService _userService = new UserService();

                var y = _userService.GetTasks(Id);
                foreach (Task t in y)
                {
                    Console.WriteLine($"Task Name:{t.Name}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }

        private static void FinishedTasksByUser()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                UserService _userService = new UserService();

                var y = _userService.GetFinishedTasks(Id);
                foreach (Tuple<int,string> t in y)
                {
                    Console.WriteLine($"Task Id:  {t.Item1} Task Name:{t.Item2}");
                }
                
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static void GetTeamsLimited()
        {
            TeamService _teamService = new TeamService();
            var y = _teamService.GetTeamsLimited();
            foreach(Tuple<int,string,List<User>> x in y)
            {
                Console.WriteLine($"TeamID:{x.Item1} Name:{x.Item2} Users:");
                foreach(User z in x.Item3)
                {
                    Console.WriteLine($"First Name:{z.FirstName} Last Name:{z.LastName}");
                }
            }
            BasicMenu();
        }

        private static void GetUsersWithTasks()
        {
            UserService _userService = new UserService();
            var y = _userService.GetUsersWithTasks();
            foreach (Tuple<string,List<Task>> x in y)
            {
                Console.WriteLine($"First Name:{x.Item1} Tasks:");
                foreach(Task z in x.Item2)
                {
                    Console.WriteLine($"Name:{z.Name}");
                }

            }
            BasicMenu();
        }

        private static void IncorectInput()
        {
            Console.WriteLine("Incorrect input");
            BasicMenu();
        }

        private static void GetUserDetails()
        {
            Console.WriteLine("User Id");
            var UserId = Console.ReadLine();

            if (int.TryParse(UserId, out int Id))
            {
                UserService _userService = new UserService();

                var y = _userService.GetUserDetails(Id);
                if (y.User==null)
                {
                    Console.WriteLine($"NOT Found record.");
                }
                else
                {
                    
                        Console.WriteLine($"First Name:{y.User.FirstName}");
                        Console.WriteLine($"Last Project:{y.Project.Name}");
                        Console.WriteLine($"Number of Tasks in a Last Project:{y.Project.Tasks.Count}");
                        Console.WriteLine($"Number of Finished Tasks :{y.Project.Tasks.Count}");
                        Console.WriteLine($"Longest Task:{y.LongestTask.Name}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        private static void GetProjectDetails()
        {
            Console.WriteLine("Project Id");
            var ProjectId = Console.ReadLine();

            if (int.TryParse(ProjectId, out int Id))
            {
                ProjectService _projectService = new ProjectService();

                var y = _projectService.GetProjectDetails(Id);
                if (y.Project == null)
                {
                    Console.WriteLine($"NOT Found record.");
                }
                else
                {
                    Console.WriteLine($"Project Name: {y.Project.Name}");
                    Console.WriteLine($"Longest Task: {y.LongestTask.Name}");
                    Console.WriteLine($"Shortest Task: {y.ShortestTask.Name}");
                    Console.WriteLine($"Users involved: {y.NumberOfPerformers}");
                }
                BasicMenu();
            }
            else
            {
                Console.WriteLine("Wrong Id");
                BasicMenu();
            }
        }
        public static void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("0 - Number of tasks in a project for a particular user.");
            Console.WriteLine("1 - List of tasks for a particular user.");
            Console.WriteLine("2 - Get list of tasks finished in 2021 by particular user.");
            Console.WriteLine("3 - Get list of temas contains users over 10 years old.");
            Console.WriteLine("4 - Get list of users with tasks assigned");
            Console.WriteLine("5 - Get User details");
            Console.WriteLine("6 - Get Project Details");

            var input = Console.ReadKey();
            switch (input.KeyChar)
            {
                case '0':
                    ProjectsByUser();
                    break;
                case '1':
                    TasksByUser();
                    break;
                case '2':
                    FinishedTasksByUser();
                    break;
                case '3':
                    GetTeamsLimited();
                    break;
                case '4':
                    GetUsersWithTasks();
                    break;
                case '5':
                    GetUserDetails();
                    break;
                case '6':
                    GetProjectDetails();
                    break;
                default:
                    IncorectInput();
                    break;
            }
        }
        private static async System.Threading.Tasks.Task PrepareData()
        {
            var projectsJson = await _client.GetStringAsync("/projects");
            var tasksJson = await _client.GetStringAsync("/tasks");
            var usersJson = await _client.GetStringAsync("/users");
            var teamsJson = await _client.GetStringAsync("/teams");
            _client.Dispose();
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(projectsJson);
            List<Task> tasks = JsonConvert.DeserializeObject<List<Task>>(tasksJson);
            List<User> users = JsonConvert.DeserializeObject<List<User>>(usersJson);
            List<Team> teams = JsonConvert.DeserializeObject<List<Team>>(teamsJson);

            Users = users.GroupJoin(tasks,
                u => u.Id,
                t => t.PerformerId,
                (u, tasks) => new User
                {
                    Id = u.Id,
                    TeamId = u.TeamId,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    RegisteredAt = u.RegisteredAt,
                    Birthday = u.Birthday,
                    Tasks = tasks.ToList()
                }).ToList();

            Teams = teams.GroupJoin(users,
               t => t.Id,
               u => u.TeamId,
               (t, users) => new Team
               {
                   Id = t.Id,
                   Name = t.Name,
                   CreatedAt = t.CreatedAt,
                   Users = users.ToList()
               }).ToList();

            Tasks = tasks;

            Projects = projects.GroupJoin(tasks,
                p => p.Id,
                t => t.ProjectId,
                (p, tasks) => new Project
                {
                    Id = p.Id,
                    AuthorId = p.AuthorId,
                    Name = p.Name,
                    Description = p.Description,
                    CreatedAt = p.CreatedAt,
                    Deadline = p.Deadline,
                    Tasks = (from t in tasks
                             select new Task
                             {
                                 ProjectId=t.ProjectId,
                                 PerformerId=t.PerformerId,
                                 Performer= (from u in Users
                                             where u.Id == t.PerformerId
                                             select (u)).FirstOrDefault(),
                                 Name=t.Name,
                                 Description=t.Description,
                                 State=t.State,
                                 CreatedAt=t.CreatedAt,
                                 FinishedAt=t.FinishedAt

                             }).ToList(),
                    Author=(from u in Users
                            where u.Id==p.AuthorId
                            select (u)).FirstOrDefault(),
                    TeamId=p.TeamId,
                    Team=(from t in Teams
                          where t.Id==p.Id
                          select(t)).FirstOrDefault()

                }).ToList();

           

            
        }
        public static List<Project> Projects { get; private set; }
        public static List<Task> Tasks { get; private set; }
        public static List<User> Users { get; private set; }
        public static List<Team> Teams { get; private set; }
    }
}
